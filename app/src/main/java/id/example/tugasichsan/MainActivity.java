package id.example.tugasichsan;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import id.example.tugasichsan.adapter.BatikAdapter;
import id.example.tugasichsan.model.Batiks;

public class MainActivity extends AppCompatActivity {

    private RecyclerView listRv;
    private List<Batiks> list;
    private RecyclerView.ItemDecoration decoration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        setupList();
        listRv = findViewById(R.id.rv_batik);
        setupRecyclerview();

    }

    private void setupRecyclerview() {

        BatikAdapter adapter = new BatikAdapter(list);
        listRv.setLayoutManager(new LinearLayoutManager(this));
        decoration = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        listRv.addItemDecoration(decoration);
        listRv.setAdapter(adapter);
    }

    private void setupList() {
        list = new ArrayList();
        list.add(new Batiks("Batik Tujuh Rupa", "Pekalongan",
                "https://4.bp.blogspot.com/-oY7M9zo7930/WAudvhSAWDI/AAAAAAAABpY/QF514baWMokohMzKFl5PG3xgvvW9KQ9MQCLcB/s1600/2-motif-batik-tujuh-rupa.jpg"));

        list.add(new Batiks("Batik Parang", "Jawa",
                "https://asset-a.grid.id/crop/0x0:0x0/700x0/photo/2018/10/01/2293961216.jpg"));

        list.add(new Batiks("Batik Priyangan", "Tasikmalaya",
                "https://cdn2.tstatic.net/travel/foto/bank/images/batik-priyangan_20181002_091019.jpg"));

        list.add(new Batiks("Batik Pring Sedapur", "Magetan",
                "http://www.gambarbagus.com/wp-content/uploads/2017/11/Motif-Batik-Pring-Sedapur-Magetan.png"));

        list.add(new Batiks("Batik Sogan", "Solo",
                "https://cdn2.tstatic.net/travel/foto/bank/images/motif-batik-sogan_20181002_090427.jpg"));

        list.add(new Batiks("Batik Simbut", "Banten",
                "https://cdn.shopify.com/s/files/1/0366/9825/files/29_-_banten.png?v=1499266790"));

        list.add(new Batiks("Batik  Mega Mendung", "Cirebon",
                "https://batik-tulis.com/wp-content/uploads/2015/12/batik-mega-mendung.jpg"));

        list.add(new Batiks("Batik Keraton", "Yogyakarta",
                "https://4.bp.blogspot.com/-I93HAg03bew/V_Cy-QZHj3I/AAAAAAAACzs/80xrNwIwHc0ZDrcGFMbponVBpxXj_axOACLcB/s1600/batik%2Bkeraton.jpg"));
    }


}
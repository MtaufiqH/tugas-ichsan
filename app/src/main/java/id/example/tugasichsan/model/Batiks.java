package id.example.tugasichsan.model;

public class Batiks {
    private String namaBatik;
    private String asalBatik;
    private String imageUrl;

    public Batiks(String namaBatik, String asalBatik, String imageUrl) {
        this.namaBatik = namaBatik;
        this.asalBatik = asalBatik;
        this.imageUrl = imageUrl;
    }

    public String getNamaBatik() {
        return namaBatik;
    }


    public String getAsalBatik() {
        return asalBatik;
    }


    public String getImageUrl() {
        return imageUrl;
    }

}


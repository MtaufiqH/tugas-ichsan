package id.example.tugasichsan.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

import id.example.tugasichsan.R;
import id.example.tugasichsan.model.Batiks;

public class BatikAdapter extends RecyclerView.Adapter<BatikAdapter.BatikViewHolder> {

    private List<Batiks> list;

    public BatikAdapter(List<Batiks> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public BatikViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new BatikViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.batik_row, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull BatikViewHolder holder, int position) {
        holder.bindItem(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class BatikViewHolder extends RecyclerView.ViewHolder {
        ImageView gambarBatik;
        TextView namaBatik;
        TextView asalBatik;

        public BatikViewHolder(@NonNull View itemView) {
            super(itemView);

            namaBatik = itemView.findViewById(R.id.tv_nama_batik);
            asalBatik = itemView.findViewById(R.id.tv_asal_batik);
            gambarBatik = itemView.findViewById(R.id.iv_batik);

        }

        public void bindItem(final Batiks item) {
            Glide.with(itemView.getContext()).load(item.getImageUrl()).into(gambarBatik);
            namaBatik.setText(item.getNamaBatik());
            asalBatik.setText(item.getAsalBatik());
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(itemView.getContext(), "you're clicked " + item.getNamaBatik(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
}
